
# Notes

## EXAMPLE

```go
mariaConnection := MariaDBConf{}
mariaConnection.Init("test", "127.0.0.1", "3306")
mariaConnection.Connect("username", "password")
defer mariaConnection.Close()

for _, valuePair := range mariaConnection.GetHashes() {

	redisConnection.MapInsert("users", valuePair.Name, valuePair.PasswordHash)
}
```

## SETTING UP MARIADBs

check docs [here](https://gitlab.com/minimmoe/docs/blob/master/mariadb.md)

## Maria Golang component notes

MariaDB username and password are the same ones that are in your docker mariadb. (Going with defaults, user should be root and password should be whatever you pass to MYSQL_ROOT_PASSWORD environment variable when you run mariadb container)

Database is what you pass to MYSQL_DATABASE environment variable

## Sources

https://github.com/go-sql-driver/mysql

https://godoc.org/github.com/go-sql-driver/mysql

https://www.youtube.com/watch?v=DWNozbk_fuk

https://mariadb.com/resources/blog/using-go-with-mariadb/

mariadb sql driver tlsconfig (example has client certs for 2 way encryption)
https://godoc.org/github.com/go-sql-driver/mysql#RegisterTLSConfig