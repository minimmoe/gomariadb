package gomariadb

import (
	"crypto/tls"
	"database/sql"
	"errors"

	"github.com/go-sql-driver/mysql"
)

// MariaDBConf MariaDB shared data
type MariaDBConf struct {
	DBConnection *sql.DB
}

// TestConnection starts and stores a MariaDB database connection.
func TestConnection(Username, Password, DatabaseName, ConnectionIP, ConnectionPort string) error {
	mariaDB := new(MariaDBConf)
	mariaDB.Connect(Username, Password, DatabaseName, ConnectionIP, ConnectionPort)
	defer mariaDB.Close()

	// check that database is reachable
	if err := mariaDB.DBConnection.Ping(); err != nil {
		return errors.New("MariaDB not reached.\n " + err.Error())
	}

	return nil
}

// Connect stores a MariaDB database connection for later use.
// does not actually connect. Connection happens when inserting data.
func (M *MariaDBConf) Connect(Username, Password, DatabaseName, ConnectionIP, ConnectionPort string) error {

	mysql.RegisterTLSConfig("custom", &tls.Config{
		InsecureSkipVerify: true,
	})

	// "username:password@tcp(ip:port)/databasename?tls=custom"
	connectionsString := Username + ":" + Password + "@" + "tcp" +
		"(" + ConnectionIP +
		":" + ConnectionPort + ")/" +
		DatabaseName + "?tls=custom"

	var err error
	// Create the database handle, confirm driver is present
	M.DBConnection, err = sql.Open("mysql", connectionsString) // mysql works for mariadb
	if err != nil {
		return err
	}
	return nil
}

// GetHash returns a single password hash based on a username or an error.
func (M *MariaDBConf) GetHash(username string) (string, error) {

	var hash string
	stmtOut, err := M.DBConnection.Prepare("select password from auth_user where username=?")
	if err != nil {
		return "", err
	}
	defer stmtOut.Close()

	err = stmtOut.QueryRow(username).Scan(&hash)
	if err != nil {
		return "", err
	}
	return hash, nil
}

func (M *MariaDBConf) getUserID(username string) (string, error) {

	var uid string
	stmtOut, err := M.DBConnection.Prepare("SELECT id FROM auth_user WHERE username=?;")
	if err != nil {
		return "", err
	}
	defer stmtOut.Close()

	err = stmtOut.QueryRow(username).Scan(&uid)
	if err != nil {
		return "", err
	}
	return uid, nil
}

// GetBans returns all bans on a username or an error.
func (M *MariaDBConf) GetBans(username string) ([]string, error) {

	uid, err := M.getUserID(username)
	if err != nil {
		return nil, err
	}

	stmtOut, err := M.DBConnection.Prepare("SELECT BanReason, BanExpiration, PermaBan FROM Bans WHERE auth_user_id=? AND (BanExpiration > CURDATE() OR PermaBan = true);")
	if err != nil {
		return nil, err
	}
	defer stmtOut.Close()

	rows, err := stmtOut.Query(uid)
	if err != nil {
		return nil, err
	}

	var bans []string
	for rows.Next() {
		var reason string
		var expires string
		var permanent bool

		rows.Scan(&reason, &expires, &permanent)

		if permanent {
			bans = append(bans, reason+". Ban is permanent.")
		} else {
			bans = append(bans, reason+". Ban expires: "+expires)
		}
	}

	return bans, nil
}

// Close is called manually so it can be ended in main loop
func (M *MariaDBConf) Close() error {

	err := M.DBConnection.Close()
	if err != nil {
		return err
	}
	return nil
}
